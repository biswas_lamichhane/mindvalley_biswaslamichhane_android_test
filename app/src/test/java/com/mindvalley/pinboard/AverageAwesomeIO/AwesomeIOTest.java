package com.mindvalley.pinboard.AverageAwesomeIO;

import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.AIOBaseRequest;
import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.JSONArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by biswas on 20/08/16.
 */
public class AwesomeIOTest{



    @Test
    public void testProcessRequestInvalidResource() throws Exception {

        JSONArrayRequest request = new JSONArrayRequest();
        request.setUrl("http://google.com"); //Invalid url
        request.setRefresh(false);
        request.setType(AIOBaseRequest.REQ_TYPE_GET);
        request.setHandler(new JSONArrayRequest.JSONResponseHandler() {
            @Override
            public void onNetworkFailure(String failure) {
                Assert.assertTrue("Network Failure", true);
            }

            @Override
            public void onResponseFailure(String failure, int code) {
                Assert.assertTrue("Response Failure", true);
            }

            @Override
            public void onParseError(String error) {
                Assert.assertTrue("Parse Error", true);
            }

            @Override
            public void onReceiveJSONArray(JSONArray array) {
                Assert.fail("Cannot come here if invalid url.");
            }

            @Override
            public void onReceiveJSONObject(JSONObject jsonObject) {
                Assert.fail("Cannot come here if invalid url");
            }
        });

        AwesomeIO.getInstance().processRequest(request);


    }


}