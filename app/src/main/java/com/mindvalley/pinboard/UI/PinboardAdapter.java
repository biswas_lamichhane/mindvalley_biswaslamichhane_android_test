package com.mindvalley.pinboard.UI;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindvalley.pinboard.AverageAwesomeIO.AwesomeImageLoader;
import com.mindvalley.pinboard.Models.BoardItem;
import com.mindvalley.pinboard.R;

import java.util.List;

/**
 * Created by biswas on 19/08/16.
 */
public class PinboardAdapter extends RecyclerView.Adapter<PinboardAdapter.PinItem> {

    private Context context;
    private List<BoardItem> pins;

    public PinboardAdapter(Context context, List<BoardItem> pins) {
        this.context = context;
        this.pins = pins;
    }

    @Override
    public PinItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pin_item, parent, false);
        PinItem pinView = new PinItem(layoutView);
        return pinView;
    }

    @Override
    public void onBindViewHolder(PinItem holder, int position) {
        BoardItem item = pins.get(position);

        holder.imageView.setImageResource(R.drawable.userhead);

        AwesomeImageLoader.
                makeLoader().
                withPlaceHolderImage(R.drawable.ic_placeholder).
                withErrorImage(R.drawable.ic_img_error).
                toImageView(holder.imageView).load(item.urls.smallURL);

        String cats = "";
        for(int i = 0; i<item.categories.size();i++) {
            if(i == 0)
                cats += (item.categories.get(i).title);
            else cats += ", "+item.categories.get(i).title;
        }

        holder.categoriesView.setText(cats);
        holder.likeCountView.setText(String.valueOf(item.likes));

        if(item.liked)
            holder.favIcon.setImageResource(R.drawable.ic_fav_liked);
        else holder.favIcon.setImageResource(R.drawable.ic_fav_unliked);

        holder.nameView.setText(item.user.name);
        holder.usernameView.setText(item.user.username);

        holder.userImageView.setImageResource(R.drawable.userhead);

        //AwesomeIO.getInstance().loadImageToView(item.user.profileImage.small,R.drawable.userhead, R.drawable.userhead, holder.userImageView);
        AwesomeImageLoader.
                makeLoader().
                withPlaceHolderImage(R.drawable.userhead).
                withErrorImage(R.drawable.ic_img_error).
                toImageView(holder.userImageView).load(item.user.profileImage.small);
    }

    @Override
    public int getItemCount() {
        return pins.size();
    }

    class PinItem extends RecyclerView.ViewHolder {
        ImageView imageView, favIcon, userImageView;
        TextView likeCountView, categoriesView, usernameView, nameView;

        public PinItem(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.img);
            favIcon = (ImageView) itemView.findViewById(R.id.fav_img);
            likeCountView = (TextView) itemView.findViewById(R.id.likeCount);
            categoriesView = (TextView) itemView.findViewById(R.id.categories);
            userImageView  = (ImageView) itemView.findViewById(R.id.usr_img);
            nameView = (TextView) itemView.findViewById(R.id.name);
            usernameView = (TextView) itemView.findViewById(R.id.username);

        }
    }

    public void resetItems(List<BoardItem> items){
        this.pins = items;
        this.notifyDataSetChanged();
    }

}