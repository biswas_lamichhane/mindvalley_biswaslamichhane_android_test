package com.mindvalley.pinboard.UI.Activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.mindvalley.pinboard.AverageAwesomeIO.AwesomeIO;
import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.AIOBaseRequest;
import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.JSONArrayRequest;
import com.mindvalley.pinboard.Models.BoardItem;
import com.mindvalley.pinboard.Models.Feed;
import com.mindvalley.pinboard.R;
import com.mindvalley.pinboard.UI.PinItemsDecor;
import com.mindvalley.pinboard.UI.PinboardAdapter;

import org.json.JSONArray;

import java.util.ArrayList;

public class PinBoardActivity extends AppCompatActivity implements JSONArrayRequest.JSONArrayResponseHandler {

    FloatingActionButton fab;
    RecyclerView mRecyclerView;
    private SwipeRefreshLayout swipeLayout;
    PinboardAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_board);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(fab, "No action assigned to this FAB.", Snackbar.LENGTH_LONG)
                        .show();
            }
        });

        setupSwipeToRefresh();

        mRecyclerView = (RecyclerView) findViewById(R.id.pinBoard);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        }else mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        adapter = new PinboardAdapter(this, new ArrayList<BoardItem>());
        mRecyclerView.setAdapter(adapter);
        PinItemsDecor decoration = new PinItemsDecor(10);
        mRecyclerView.addItemDecoration(decoration);


        /* Cache Size configuration
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        AvgAwesome.getInstance().configureCache(AvgAwesome.CACHE_TYPE_BITMAP, maxMemory/4); */

        loadFeed(false);
    }


    private void setupSwipeToRefresh(){

        // Lookup the swipe container view
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Load the feed on pull-to-refresh action
                loadFeed(false);
            }
        });

        // Configure the refreshing colors
        swipeLayout.setColorSchemeResources(
                R.color.colorPrimary,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }



    private void loadFeed(boolean isRefresh){
        swipeLayout.setRefreshing(true);
        JSONArrayRequest request = new JSONArrayRequest();
        request.setUrl("http://pastebin.com/raw/wgkJgazE");
        request.setParameters(null);
        request.setRefresh(isRefresh);
        request.setType(AIOBaseRequest.REQ_TYPE_GET);
        request.setHandler(this);
        AwesomeIO.getInstance().processRequest(request);
    }



    @Override
    public void onNetworkFailure(final String failure) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(fab, failure, Snackbar.LENGTH_LONG)
                        .show();
                swipeLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResponseFailure(final String failure, final int code) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(fab, failure+" Response Code: "+code, Snackbar.LENGTH_LONG)
                        .show();
                swipeLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onParseError(final String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(fab, "Parse Error: "+error, Snackbar.LENGTH_LONG)
                        .show();
                swipeLayout.setRefreshing(false);
            }
        });


    }

    @Override
    public void onReceiveJSONArray(final JSONArray array) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
                try {
                    adapter.resetItems(Feed.parseFeed(array));
                }catch (Exception e){
                    e.printStackTrace();
                    Snackbar.make(fab, "Parse Error: "+e.getMessage(), Snackbar.LENGTH_LONG)
                            .show();
                    swipeLayout.setRefreshing(false);

                }

            }
        });

    }

}
