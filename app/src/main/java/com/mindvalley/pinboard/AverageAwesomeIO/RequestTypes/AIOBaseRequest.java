package com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by biswas on 20/08/16.
 * Base Class for all the AwesomeIO
 * request types.
 *
 *
 */

public abstract class AIOBaseRequest{


    /***
     * Static mehtods and Fields
     */

     public static final String REQ_TYPE_GET = "GET";
     public static final String REQ_TYPE_POST = "POST";

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");


    /***
     * AAIORequest methods & Properties
     */

    /* Request URL */
    String url;

    /* Post Parameters Hash */
    HashMap<String, String> parameters;

    /* Request Type */
    String type;

    /* Request Cache Policy: isRefresh true if no need to look cache. */
    boolean isRefresh;


    /* OkHttp call object belonging to this request*/
    Call thisCall;


    /***
     * Abstract method for handling failure.
     * Should be implemented by the subclasses of AIOBaseRequest
     * @param call
     * @param e
     */
    public abstract void handleFailure(Call call, Exception e);


    /***
     * Abstract method for handling response.
     * Should be implemented by the subclasses of AIOBaseRequest
     * @param call
     * @param response
     */
    public abstract void handleResponse(Call call, Object response);

    /***
     * Abstract method for handling cached data.
     * Should be implemented by the subclasses of AIOBaseRequest
     * @param cache
     */
    public abstract void handleCachedResponse(Object cache);


    /***
     * Sets call object for this request.
     * @param c
     */
    public void setCall(Call c){
        this.thisCall = c;
    }

    /***
     * Cancels this request
     * if it is not already cancelled, or executed.
     */
    public void cancelRequest(){
        if(!thisCall.isCanceled() && !thisCall.isExecuted())
            thisCall.cancel();
    }

    /***
     * For post requests, we might need to send body.
     * If parameters are set, this method will return body as a JSON string.
     * Currently only supports JSON type posting but can be extended here.
     */
     public RequestBody getBody(){

         if(this.type.contentEquals(AIOBaseRequest.REQ_TYPE_POST) && this.parameters!=null)
            return RequestBody.create(JSON, new JSONObject(this.parameters).toString());
         else return null;
     }

    /***
     * General getter & setter methods
     */

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isRefresh() {
        return isRefresh;
    }

    public void setRefresh(boolean refresh) {
        isRefresh = refresh;
    }



}
