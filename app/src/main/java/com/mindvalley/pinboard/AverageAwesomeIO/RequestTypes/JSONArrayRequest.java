package com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes;

import org.json.JSONArray;
import org.json.JSONException;

import okhttp3.Call;

/**
 * Created by biswas on 20/08/16.
 */
public class JSONArrayRequest extends AIOBaseRequest {


    /* Data-Specific handler: To be implemented by the user */
    public JSONArrayResponseHandler mHandler;

    public void setHandler(JSONArrayResponseHandler mHandler) {
        this.mHandler = mHandler;
    }

/* Implementation - Response Handling */

    @Override
    public void handleFailure(Call call, Exception e) {
        mHandler.onNetworkFailure("Network or I/O Error: "+e.getMessage());
    }

    @Override
    public void handleResponse(Call call, Object response) {
            /* Try converting this to JSON */
            try {
                String resp = (String) response;
                JSONArray json = new JSONArray(resp);
                mHandler.onReceiveJSONArray(json);
            } catch (JSONException e) {
                e.printStackTrace();
                mHandler.onParseError("Parse Error: "+e.getMessage());
            }
    }

    @Override
    public void handleCachedResponse(Object cacheData) {
        try {
            String data = (String) cacheData;
            mHandler.onReceiveJSONArray(new JSONArray(data));
        }catch(JSONException e){
            e.printStackTrace();
            mHandler.onParseError("Parse Error: "+e.getMessage());
        }
    }

    /**
     *  Interfaces:
     *
     * JSONArrayResponseHandler Interface:
     *  It mainly deals with conveying the responses or failures
     *  from the library.
     */

    public interface JSONArrayResponseHandler{
        public void onNetworkFailure(String failure);
        public void onResponseFailure(String failure,int code);
        public void onParseError(String error);
        public void onReceiveJSONArray(JSONArray array);
    }

}
