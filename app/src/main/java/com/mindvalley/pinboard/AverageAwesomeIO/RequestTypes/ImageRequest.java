package com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes;

import android.graphics.Bitmap;

import okhttp3.Call;

/**
 * Created by biswas on 20/08/16.
 *
 * Class used for making a Image Request.
 *
 */
public class ImageRequest extends AIOBaseRequest {

    /* Data-Specific handler: To be implemented by the user */
    public ImageResponseHandler mHandler;
    public void setHandler(ImageResponseHandler mHandler) {
        this.mHandler = mHandler;
    }


    /* Implementation - Response Handling from Super Class AIOBaseRequest */
    @Override
    public void handleFailure(Call call, Exception e) {
        mHandler.onNetworkFailure("Network or I/O Error: "+e.getMessage());
    }

    @Override
    public void handleResponse(Call call, Object response) {

            /* Cast response to bitmap */
            Bitmap bmp = (Bitmap) response;
            mHandler.onReceiveBitmap(bmp);

    }

    @Override
    public void handleCachedResponse(Object cachedImage) {
       Bitmap bmp = (Bitmap) cachedImage;
       mHandler.onReceiveBitmap(bmp);
    }


    /***
     * Interface for handling image request's responses.
     */
    public interface ImageResponseHandler{
        public void onNetworkFailure(String failure);
        public void onResponseFailure(String failure,int code);
        public void onReceiveBitmap(Bitmap bitmap);
    }


}
