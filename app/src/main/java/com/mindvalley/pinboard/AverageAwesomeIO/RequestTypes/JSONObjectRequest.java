package com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

/**
 * Created by biswas on 20/08/16.
 */
public class JSONObjectRequest extends AIOBaseRequest {

    /* Data-Specific handler: To be implemented by the user */
    public JSONObjectResponseHandler mHandler;

    public void setHandler(JSONObjectResponseHandler mHandler) {
        this.mHandler = mHandler;
    }

    /* Implementation - Response Handling */

    @Override
    public void handleFailure(Call call, Exception e) {
        mHandler.onNetworkFailure("Network or I/O Error: "+e.getMessage());
    }

    @Override
    public void handleResponse(Call call, Object response) {

                            /* Try converting this to JSON */
            try {
                String resp = (String) response;
                JSONObject json = new JSONObject(resp);
                mHandler.onReceiveJSONObject(json);
            } catch (JSONException e) {
                mHandler.onParseError("Parse Error: "+e.getMessage());
            }


    }

    @Override
    public void handleCachedResponse(Object cacheData) {
        try {
            String data =(String) cacheData;
            mHandler.onReceiveJSONObject(new JSONObject(data));
        }catch(JSONException e){
            e.printStackTrace();
            mHandler.onParseError("Parse Error: "+e.getMessage());
        }
    }


    public interface JSONObjectResponseHandler{
        public void onNetworkFailure(String failure);
        public void onResponseFailure(String failure,int code);
        public void onParseError(String error);
        public void onReceiveJSONObject(JSONObject jsonObject);
    }

}
