package com.mindvalley.pinboard.AverageAwesomeIO;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.LruCache;

import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.AIOBaseRequest;
import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.ImageRequest;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by biswas on 19/08/16.
 *
 * This class is a singleton Class that receives, processes & requests all the HTTP requests
 *
 */

public class AwesomeIO {

    public static final int CACHE_TYPE_BITMAP = 1;
    public static final int CACHE_TYPE_GENERAL = 2;



    /* Singleton Instance */
    private static AwesomeIO mInstance = null;

    /* OKHTTP client object */
    private OkHttpClient mClient;

    /* Memory cache to cache Bitmaps*/
    private LruCache<String, Bitmap> mBitmapCache;

    /* Memory cache to cache String/JSON Data */
    private LruCache<String, String> mGeneralCache;

    protected AwesomeIO(){
        mClient = new OkHttpClient();

        /***
         *  Initial configuration of the cache:
         *  maxMemory: Maximum heap memory.
         *  bmpMemory: Bitmap cache memory: 1/8th of the available memory.
         *  generalMem: General Cache memory: 1/16th of available memory.
         */


        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int bmpMemory = maxMemory / 8;
        final int generalMem = maxMemory / 16;

        configureCache(CACHE_TYPE_BITMAP,bmpMemory);
        configureCache(CACHE_TYPE_GENERAL, generalMem);
    }


    /***
     * Method that configures the bitmap and general in-memory caches.
     * @param cacheType
     * @param cacheSize
     */
    public void configureCache(int cacheType,int cacheSize){
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
         cacheSize = maxMemory / 8;

        if(cacheType == 1) {
            mBitmapCache = new LruCache<String, Bitmap>(cacheSize){
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    // The cache size will be measured in kilobytes rather than
                    // number of items.
                    return bitmap.getByteCount() / 1024;
                }
            };
        }else if(cacheType == 2){
            mGeneralCache = new LruCache<String, String>(cacheSize){
                @Override
                protected int sizeOf(String key, String data) {
                    // The cache size will be measured in kilobytes rather than
                    // number of items.
                    return data.getBytes().length / 1024;
                }
            };
        }
    }


    /***
     * Method that adds bitmap to lru cache.
     * @param key
     * @param bitmap
     */
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mBitmapCache.put(key, bitmap);
        }
    }

    /***
     * Method that gets bitmap from cache for an url
     * @param key
     * @return
     */
    public Bitmap getBitmapFromMemCache(String key) {
        return mBitmapCache.get(key);
    }


    /***
     * Method that adds String/JSON data to cache.
     * @param key
     * @param data
     */
    public void addDataToMemoryCache(String key, String data) {
        if (getDataFromMemoryCache(key) == null) {
            Log.d("GeneralCache","Cache added for: "+key);
            mGeneralCache.put(key, data);
        }
    }

    /***
     * Method that gets String/JSON data from cache.
     * @param key
     * @return
     */
    public String getDataFromMemoryCache(String key) {
        Log.d("GeneralCache","Looking for: "+key+" in cache.");
        return mGeneralCache.get(key);
    }


    /* Singleton method to get Instance of AvgAwesome Client */
    public static synchronized AwesomeIO getInstance(){
        if(mInstance==null)
            mInstance = new AwesomeIO();
        return mInstance;
    }

    /***
     * Process JSON/String requests/
     * This method is responsible for retreiving & caching JSON/String & other
     * types of requests.
     * @param r
     */
    public void processRequest(final AIOBaseRequest r){

        Request request =new Request.Builder()
                .url(r.getUrl())
                //.post(r.getBody())
                .build();

        Call call = mClient.newCall(request);
        r.setCall(call);

        String data = getDataFromMemoryCache(r.getUrl());

        if( data==null) {
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    r.handleFailure(call,e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String resp = response.body().string();
                        addDataToMemoryCache(r.getUrl(), resp);
                        r.handleResponse(call, resp);
                    }else{
                        r.handleFailure(call,new IOException("Response Error: "+response.code()));
                    }
                }
            });

        }else{
           r.handleCachedResponse(data);
        }

    }


    /***
     * This method is responsible for retreiving & caching Image requests.
     * @param r
     */
    public void processReqeust(final ImageRequest r){

        Request.Builder rb =new Request.Builder()
                .url(r.getUrl());

        if(r.getBody()!=null)
            rb.post(r.getBody());

        Request request = rb.build();

        Call call = mClient.newCall(request);
        r.setCall(call);

        Bitmap data = getBitmapFromMemCache(r.getUrl());

        if( data==null) {
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    r.handleFailure(call,e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (response.isSuccessful()) {
                        /* Convert response data to Bitmap */
                        try {
                            Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                            addBitmapToMemoryCache(r.getUrl(), bmp);
                            r.handleResponse(call, bmp);
                        }catch(Exception e){

                            /*We might get into an exception while converting to bitmap.
                            * In such case, return exception with handler's handle failure
                            */

                            e.printStackTrace();
                            r.handleFailure(call, e);
                        }
                    } else {
                        r.handleFailure(call,new IOException("Response Error: "+response.code()));
                    }

                }
            });

        }else{
            r.handleCachedResponse(data);
        }
    }

}
