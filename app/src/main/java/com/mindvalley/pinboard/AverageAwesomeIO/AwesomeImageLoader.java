package com.mindvalley.pinboard.AverageAwesomeIO;

import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;

import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.AIOBaseRequest;
import com.mindvalley.pinboard.AverageAwesomeIO.RequestTypes.ImageRequest;

/**
 * Created by biswas on 20/08/16.
 *
 * AwesomeImageLoader
 *
 * A handy class that loads bitmap from url/cache using the
 * "AwesomeIO" engine and loads the images to the specific image-view.
 */
public class AwesomeImageLoader implements ImageRequest.ImageResponseHandler{

    /* Make the Loader */
    public static AwesomeImageLoader makeLoader(){
        return new AwesomeImageLoader();
    }


    // Placeholder image resource id.
    int placeholderResource;

    //Error image res id.
    int errorResource;

    //Imageview to load the bitmap to
    ImageView loadToView;


    private AwesomeImageLoader(){
    }

    /***
     * sets placeholder resource
     * @param placeholderResource
     * @return
     */
    public AwesomeImageLoader withPlaceHolderImage(int placeholderResource){
        this.placeholderResource = placeholderResource;
        return this;
    }

    /***
     * Sets error image resource
     * @param errorResource
     * @return
     */
    public AwesomeImageLoader withErrorImage(int errorResource){
        this.errorResource = errorResource;
        return this;
    }

    /***
     * Set the imageview to load bitmap into.
     * @param imageView
     * @return
     */
    public AwesomeImageLoader toImageView(ImageView imageView){
        this.loadToView = imageView;
        return this;
    }


    /**
     * This method creates a image request and loads bitmap from the url and displays it
     * to the configured imageview.
     * @param url
     */
    public void load(String url){
        ImageRequest imgReq = new ImageRequest();
        imgReq.setUrl(url);
        imgReq.setHandler(this);
        imgReq.setParameters(null);
        imgReq.setType(AIOBaseRequest.REQ_TYPE_GET);
        imgReq.setRefresh(false);

        this.loadToView.setImageResource(this.placeholderResource);

        /* Start the reqeust now! */
        AwesomeIO.getInstance().processReqeust(imgReq);
    }


    /***
     * Implementation of ImageResponseHandler:
     * Handler network errors.
     * @param failure
     */
    @Override
    public void onNetworkFailure(String failure) {
        if(loadToView.getContext()!=null) {
            Handler h = new Handler(loadToView.getContext().getMainLooper());

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    loadToView.setImageResource(errorResource);
                }
            };

            h.post(r);
        }
    }

    /***
     * Implementation of ImageResponseHandler:
     * Handler response failures & server errors.
     * @param failure
     * @param code
     */
    @Override
    public void onResponseFailure(String failure, int code) {
        if(loadToView.getContext()!=null) {
            Handler h = new Handler(loadToView.getContext().getMainLooper());

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    loadToView.setImageResource(errorResource);
                }
            };

            h.post(r);
        }
    }

    /***
     * Implementation of ImageResponseHandler:
     * Sets bitmap to requested imageview after
     * loading. It uses the android OS's handler
     * class to gain access to the main UI thread.
     *
     * @param bitmap
     */
    @Override
    public void onReceiveBitmap(final Bitmap bitmap) {

        if(loadToView.getContext()!=null) {
            Handler h = new Handler(loadToView.getContext().getMainLooper());

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    loadToView.setImageBitmap(bitmap);
                }
            };

            h.post(r);
        }

    }


}
