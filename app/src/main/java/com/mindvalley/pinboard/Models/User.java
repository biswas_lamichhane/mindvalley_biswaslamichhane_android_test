package com.mindvalley.pinboard.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by biswas on 19/08/16.
 */
public class User {

    public String id, username, name;

    @SerializedName("profile_image")
    public ProfileImage profileImage;

    @SerializedName("links")
    public ProfileLinks links;

    public class ProfileImage{

        public String small, medium, large;
    }

    public class ProfileLinks{
        public String self, html, photos, likes;
    }
}
