package com.mindvalley.pinboard.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by biswas on 19/08/16.
 */
public class Category {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("photo_count")
    public int photoCount;

    @SerializedName("links")
    public CatLinks links;


    public class CatLinks{
        @SerializedName("self")
        public String self;

        @SerializedName("photos")
        public String photos;

    }

}
