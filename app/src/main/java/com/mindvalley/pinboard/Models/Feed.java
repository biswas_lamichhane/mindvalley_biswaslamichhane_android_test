package com.mindvalley.pinboard.Models;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;

/**
 * Created by biswas on 19/08/16.
 */
public class Feed {

    public static List<BoardItem> parseFeed(JSONArray feedArray){
        Log.d("ParseFeed","Array: "+feedArray.toString());
        Gson gson = new GsonBuilder().create();
        BoardItem[] items = gson.fromJson(feedArray.toString(), BoardItem[].class);
        return Arrays.asList(items);
    }
}
