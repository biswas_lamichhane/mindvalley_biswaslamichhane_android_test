package com.mindvalley.pinboard.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by biswas on 19/08/16.
 */
public class BoardItem {


    @SerializedName("id")
    public String id;

    @SerializedName("created_at")
    public String created;

    @SerializedName("width")
    public double width;

    @SerializedName("height")
    public double height;

    @SerializedName("color")
    public String color;

    @SerializedName("likes")
    public int likes;

    @SerializedName("liked_by_user")
    public boolean liked;


    @SerializedName("urls")
    public ItemURLS urls;

    @SerializedName("links")
    public ItemLinks links;

    @SerializedName("categories")
    public List<Category> categories;

    @SerializedName("user")
    public User user;






    public class ItemURLS{

        @SerializedName("raw")
        public String rawURL;

        @SerializedName("full")
        public String fullURL;

        @SerializedName("regular")
        public String regularURL;

        @SerializedName("small")
        public String smallURL;

        @SerializedName("thumb")
        public String thumbURL;




    }

    public class ItemLinks{

        @SerializedName("self")
        public String self;

        @SerializedName("html")
        public String html;

        @SerializedName("download")
        public String download;



    }


}
